CREATE DATABASE IF NOT EXISTS `easyfresh-master`;

DROP TABLE `order_items`;
DROP TABLE `orders`;
DROP TABLE `products`;
CREATE TABLE `products` (
	`name` varchar(255) primary key,
	`unit` varchar(255) not null,
	`price` DECIMAL(11,3) unsigned NOT NULL
);

CREATE TABLE `orders` (
	`id` INT unsigned PRIMARY KEY AUTO_INCREMENT,
	`customer_name` varchar(255) not null,
	`delivery_date` bigint(11) not null,
	`delivery_address` varchar(255) not null,
	`order_taker_name` varchar(255),
	`create_at` TIMESTAMP DEFAULT (NOW())
);

CREATE TABLE `order_items` (
	`order_id` INT unsigned,
	`product_name` varchar(255),
	`quantity` DECIMAL(11,3) unsigned NOT NULL,
	`total` DECIMAL(11,3) unsigned NOT NULL,
	`rank` INT unsigned NOT NULL
);

ALTER TABLE `order_items` ADD FOREIGN KEY (`order_id`) REFERENCES `orders`(`id`);
ALTER TABLE `order_items` ADD FOREIGN KEY (`product_name`) REFERENCES `products`(`name`);

INSERT INTO `products` (`name`, `unit`, `price`)  VALUES ('Butter', 'KG', 400);
INSERT INTO `products` (`name`, `unit`, `price`)  VALUES ('Eggs', 'Dozen', 180);
INSERT INTO `products` (`name`, `unit`, `price`)  VALUES ('Chicken', 'OZ', 8);
INSERT INTO `products` (`name`, `unit`, `price`)  VALUES ('Tomato', 'KG', 50);