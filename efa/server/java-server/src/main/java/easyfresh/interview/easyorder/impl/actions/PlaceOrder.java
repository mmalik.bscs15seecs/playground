package easyfresh.interview.easyorder.impl.actions;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.google.common.flogger.FluentLogger;
import com.google.common.primitives.UnsignedInteger;
import com.jolbox.bonecp.BoneCP;
import easyfresh.interview.easyorder.impl.DatabaseHelper;
import easyfresh.interview.easyorder.impl.ResultOr;
import easyfresh.interview.grpc.Order.OrderItem;
import easyfresh.interview.grpc.Order.Product;
import easyfresh.interview.grpc.Order.PlaceOrderResponse;
import easyfresh.interview.grpc.Order.PlaceOrderRequest;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

/** Action to perform when a request to place an order comes in. */
public class PlaceOrder {
  private static final FluentLogger LOG = FluentLogger.forEnclosingClass();
  private final BoneCP dbCxPool;
  private final String dbName;
  /** Acts as a cache store for product list. */
  private static List<Product> products = null;
  /** Acts as a key index lookup into product list. */
  private static Map<String, Integer> productKeys = null;

  public PlaceOrder(BoneCP dbCxPool, String dbName) {
    this.dbCxPool = dbCxPool;
    this.dbName = dbName;
  }

  /** Verify that the must-required fields are present in the request. */
  private boolean isOrderDetailValid(PlaceOrderRequest request) {
    boolean isInvalid = request.getCustomerName().isEmpty();
    isInvalid |= request.getCustomerAddress().isEmpty();
    isInvalid |= !isDeliveryDateWithXDays(request.getDeliveryDate(), 2);

    return !isInvalid;
  }

  /** Checks if the delivery date is within the range drom today to next X days. */
  private boolean isDeliveryDateWithXDays(long deliveryDateUnix, int xDays) {
    Instant insDeliveryDate = Instant.ofEpochSecond(deliveryDateUnix);

    // Is there a potential failure case where lat's say the client makes a request
    // at 23:59:59 but the network delay causes it to arrive here say 00:00:01 then the
    // request will be considered invalid?

    // Reset the time portion.
    Date date = new Date();
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);

    Instant minInstant = date.toInstant();
    Instant maxInstant = minInstant.plusSeconds(Duration.ofDays(xDays).getSeconds());
    return insDeliveryDate.isAfter(minInstant) && insDeliveryDate.isBefore(maxInstant);
  }

  /** Verify that the must-required fields are present in the item and the product does exist. */
  private boolean isOrderItemValid(OrderItem item) {
    boolean isInvalid = item.getProductName().isEmpty();
    isInvalid |= (item.getProductQuantity() <= 0);
    isInvalid |= (item.getRank() < 0);
    isInvalid |= !productKeys.containsKey(item.getProductName());

    return !isInvalid;
  }

  /** Inserts a row into orders table. */
  private Optional<UnsignedInteger> insertOrderDetails(Connection cx, PlaceOrderRequest request)
      throws SQLException {
    // Populate query params.
    Map<Integer, Object> values = Maps.newHashMap();
    values.put(1, request.getCustomerName());
    values.put(2, request.getDeliveryDate());
    values.put(3, request.getCustomerAddress());

    // Nullable param.
    if (!Strings.isNullOrEmpty(request.getOrderTaker())) {
      values.put(4, request.getOrderTaker());
    } else {
      values.put(4, "NULL");
    }

    // Execute the query.
    return DatabaseHelper.insert(cx, dbName, DatabaseHelper.INSERT_ORDER_DETAIL, values);
  }

  /** Inserts a row into order_items table. */
  private void insertOrderItem(Connection cx, int orderId, OrderItem item) throws SQLException {
    // Get the product given the product name.
    Product product = products.get(productKeys.get(item.getProductName()));

    // Populate query params.
    Map<Integer, Object> values = Maps.newHashMap();
    values.put(1, orderId);
    values.put(2, product.getName());
    values.put(3, item.getProductQuantity());
    values.put(4, product.getPrice() * item.getProductQuantity());
    values.put(5, item.getRank());

    // Execute the query.
    DatabaseHelper.insert(cx, dbName, DatabaseHelper.INSERT_ORDER_ITEM, values);
  }

  /** Cache the product list. */
  private void populateProducts() {
    if (products == null) {
      try (Connection cx = dbCxPool.getConnection()) {
        PreparedStatement ps = cx.prepareStatement(DatabaseHelper.LIST_PRODUCTS);
        products = DatabaseHelper.selectProducts(dbName, ps, null);

        if (!products.isEmpty()) {
          // Let's save the indexes in the hashtable for faster lookup.
          productKeys = new HashMap<>();
          for (int i = 0; i < products.size(); i++) {
            productKeys.put(products.get(i).getName(), i);
          }
        }
      } catch (SQLException sqlException) {
        LOG.atSevere().withCause(sqlException).log();
      }
    }
  }

  /**
   * OrderDetail and OrderItems are two separate transaction so if the OrderItems transaction fails
   * during processing then we cannot roll back OrderDetail since it is already done so we have to
   * delete it instead. There may be a better way to achieve this.
   */
  private void rollBack(Connection cx, int orderId) throws SQLException {
    if (!cx.getAutoCommit()) {
      // Roll back order_items transaction.
      cx.rollback();

      // Set the id for the order to be deleted from orders table.
      Map<Integer, Object> values = Maps.newHashMap();
      values.put(1, orderId);

      // Execute the query.
      DatabaseHelper.execute(cx, dbName, DatabaseHelper.DELETE_ORDER_DETAIL, values);
    }
  }

  // TODO(ammad):
  //  - Find a better pattern or collapse into a bulk query?

  /**
   * Tries to insert the order request. - First insert data in orders table. - Then insert the items
   * in order_items as a transaction to roll back in case any of insert fails.
   */
  private ResultOr<Integer> tryInsertOrder(PlaceOrderRequest request) {
    // Now check does the products exists or the order contain any items...
    if (request.getOrderItemsCount() <= 0) {
      LOG.atWarning().log();
      return new ResultOr<>(
          String.format("Failed to place an order of %d items.", request.getOrderItemsCount()));
    }

    // Insert order details.
    Optional<UnsignedInteger> orderIdOptional;
    try (Connection cx = dbCxPool.getConnection()) {
      orderIdOptional = insertOrderDetails(cx, request);
      if (orderIdOptional.isEmpty()) {
        return new ResultOr<>(
            String.format(
                "Failed to insert an order for a customer %s in the database.",
                request.getCustomerName()));
      }
    } catch (SQLException exSQL) {
      LOG.atSevere().withCause(exSQL).log();
      return new ResultOr<>(
          String.format(
              "Exception thrown while trying to place an order for customer %s.",
              request.getCustomerName()));
    }

    // Got the PK of the newly created row in the orders table.
    int orderId = orderIdOptional.get().intValue();

    // Insert order items
    try (Connection cx =
        dbCxPool.getConnection()) { // To catch exception on item insert and roll it back.
      try {
        // We will commit the transaction our self.
        cx.setAutoCommit(false);

        // Now try to insert items one by one.
        for (OrderItem item : request.getOrderItemsList()) {
          if (!isOrderItemValid(item)) {
            rollBack(cx, orderId);
            cx.setAutoCommit(true);
            return new ResultOr<>(
                String.format("Order %d contain an invalid item: %s", orderId, item));
          }
          insertOrderItem(cx, orderId, item);
        }

        // Commit it now.
        cx.commit();
        cx.setAutoCommit(true);
        LOG.atInfo().log(
            "Successfully placed an order %d for customer %s.", orderId, request.getCustomerName());
        return new ResultOr<>(orderId);

      } catch (SQLException exSQL) {
        LOG.atSevere().withCause(exSQL).log();
        rollBack(cx, orderId);
        return new ResultOr<>(
            String.format(
                "Exception thrown while trying to insert items for the order %d of customer %s.",
                orderId, request.getCustomerName()));
      }
    } catch (SQLException exSQL) {
      LOG.atSevere().withCause(exSQL).log();
      return new ResultOr<>("Failed to get connection from the pool.");
    }
  }

  public void execute(
      PlaceOrderRequest request, StreamObserver<PlaceOrderResponse> responseObserver) {
    // Cache the products if aren't already.
    populateProducts();

    if (!isOrderDetailValid(request)) {
      LOG.atWarning().log("Order detail is invalid: %s", request);
      responseObserver.onError(
          new StatusException(io.grpc.Status.INTERNAL.withDescription("Order detail is invalid.")));
      return;
    }

    ResultOr<Integer> orderIdOr = tryInsertOrder(request);
    if (!orderIdOr.ok()) {
      LOG.atWarning().log("%s", orderIdOr.detail());
      responseObserver.onError(
          new StatusException(
              io.grpc.Status.INTERNAL.withDescription("Order transaction failed.")));
      return;
    }

    PlaceOrderResponse.Builder response = PlaceOrderResponse.newBuilder();
    response.setOrderId(orderIdOr.get());
    responseObserver.onNext(response.build());
    responseObserver.onCompleted();
  }
}