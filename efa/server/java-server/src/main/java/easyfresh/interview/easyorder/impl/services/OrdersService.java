package easyfresh.interview.easyorder.impl.services;

import com.jolbox.bonecp.BoneCP;
import easyfresh.interview.easyorder.impl.actions.ListProducts;
import easyfresh.interview.grpc.Order;
import easyfresh.interview.grpc.OrdersServiceGrpc.OrdersServiceImplBase;
import easyfresh.interview.easyorder.impl.actions.PlaceOrder;
import easyfresh.interview.grpc.Order.PlaceOrderRequest;
import io.grpc.stub.StreamObserver;
import easyfresh.interview.grpc.Order.PlaceOrderResponse;

/** Service for handling the order placement requests. */
public class OrdersService extends OrdersServiceImplBase {
  private final BoneCP dbCxPool;
  private final String dbName;

  public OrdersService(BoneCP dbCxPool, String dbName) {
    this.dbCxPool = dbCxPool;
    this.dbName = dbName;
  }

  @Override
  public void placeOrder(
      PlaceOrderRequest request, StreamObserver<PlaceOrderResponse> responseObserver) {
    PlaceOrder action = new PlaceOrder(dbCxPool, dbName);
    action.execute(request, responseObserver);
  }

  @Override
  public void listProducts(
      Order.Empty request, StreamObserver<Order.ListProductsResponse> responseObserver) {
    ListProducts action = new ListProducts(dbCxPool, dbName);
    action.execute(request, responseObserver);
  }
}
