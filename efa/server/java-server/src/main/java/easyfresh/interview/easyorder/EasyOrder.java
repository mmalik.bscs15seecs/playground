package easyfresh.interview.easyorder;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.flogger.FluentLogger;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;
import easyfresh.interview.easyorder.impl.ResultOr;
import easyfresh.interview.easyorder.impl.services.OrdersService;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.Status;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/** EasyOrder is an order taking service for the EasyFresh supply-chain platform. */
public class EasyOrder {
  private static final FluentLogger LOG = FluentLogger.forEnclosingClass();
  private static final int EXIT_SUCCESS = 0;
  private static final int EXIT_FAILURE = 2;
  private static final int EXIT_ERROR = 1;

  private final Args args;
  /** To be used by grpc server. */
  private ExecutorService executorPool;

  private Server grpcServer;

  static class Args {
    @Parameter(names = {"--port"})
    private Integer port = 6060;

    @Parameter(
        names = {"--custdb-url"},
        description = "URL to the database server that contains the order tracking related tables.")
    public String custdb_url;

    @Parameter(
        names = {"--custdb-name"},
        description =
            "Name of the database schema that contains the order tracking related tables.")
    public String custdb_name = "master";

    @Parameter(
        names = {"--custdb-username"},
        description = "Name of the client to be used to connect to the database.")
    public String custdb_username;

    // TODO(ammad): Convert to a secure string
    @Parameter(
        names = {"--custdb-password"},
        description = "Password of the client to be used to connect to the database.")
    public String custdb_password;
  }

  public EasyOrder(Args args) {
    this.args = args;
  }

  public static void main(String[] argv) throws IOException, InterruptedException {
    Args args = new Args();
    JCommander.newBuilder().addObject(args).build().parse(argv);

    EasyOrder orderingServer = new EasyOrder(args);
    Status startStatus = orderingServer.start();
    if (startStatus != Status.OK) {
      LOG.atSevere().log("Failed to start: %s", startStatus);
      orderingServer.shutdown();
      System.exit(EXIT_FAILURE);
    } else {
      orderingServer.blockUntilShutdown();
      System.exit(EXIT_SUCCESS);
    }
  }

  /** Initialize the BoneCP connector. */
  private ResultOr<BoneCP> initBoneCP(String url, String username, String password) {
    BoneCPConfig config = new BoneCPConfig();
    config.setJdbcUrl(url);
    config.setUser(username);
    config.setPassword(password);

    try {
      return new ResultOr<>(new BoneCP(config));
    } catch (SQLException e) {
      return new ResultOr<>(e);
    }
  }

  private Status start() throws IOException {
    ResultOr<BoneCP> dbCxPoolOr =
        initBoneCP(args.custdb_url, args.custdb_username, args.custdb_password);
    if (!dbCxPoolOr.ok()) {
      LOG.atSevere().log("Failed to init Mariadb connection: %s", dbCxPoolOr.detail());
      System.exit(EXIT_ERROR);
    }

    BoneCP dbCxPool = dbCxPoolOr.get();

    // TODO(ammad): Play around with this and rejectionPolicy
    final int poolSize = 5;
    executorPool =
        Executors.newFixedThreadPool(
            poolSize, new ThreadFactoryBuilder().setNameFormat("rpc-executor-%d").build());

    grpcServer =
        ServerBuilder.forPort(args.port)
            .executor(executorPool)
            .addService(new OrdersService(dbCxPool, args.custdb_name))
            .build();

    grpcServer.start();
    LOG.atInfo().log(
        "EasyFreshServer started on port %d with pool size of %d.", args.port, poolSize);

    Runtime.getRuntime()
        .addShutdownHook(
            new Thread(
                () -> {
                  System.err.println("initiated shutdown");
                  this.shutdown();
                  System.err.println("shutdown completed");
                }));

    return Status.OK;
  }

  private void shutdown() {
    if (grpcServer != null) {
      grpcServer.shutdown();
    }

    if (executorPool != null) {
      executorPool.shutdownNow(); // TODO(ammad): Revisit 'shutdownNow'?
    }
  }

  private void blockUntilShutdown() throws InterruptedException {
    if (grpcServer != null) {
      grpcServer.awaitTermination();
    }
  }
}