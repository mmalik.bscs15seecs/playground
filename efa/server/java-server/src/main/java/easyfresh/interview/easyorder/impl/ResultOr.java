package easyfresh.interview.easyorder.impl;

// TODO(ammad): Look into SerenityOS patterns.
public class ResultOr<T> {
  private T result;
  private Throwable error;

  public ResultOr(T result) {
    this.error = null;
    this.result = result;
  }

  public ResultOr(Throwable error) {
    this.result = null;
    this.error = error;
  }

  public ResultOr(String detail) {
    result = null;
    this.error = new Throwable(detail);
  }

  public T get() {
    assert ok();
    return result;
  }

  public boolean ok() {
    return error == null && result != null;
  }

  public String detail() {
    if (error != null) {
      return error.getMessage();
    }
    return null;
  }

  @Override
  public String toString() {
    return String.format("[error=%s; result=%s]", error, result);
  }
}