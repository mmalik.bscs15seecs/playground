package easyfresh.interview.easyorder.impl.actions;

import com.google.common.flogger.FluentLogger;
import com.jolbox.bonecp.BoneCP;
import easyfresh.interview.easyorder.impl.DatabaseHelper;
import easyfresh.interview.grpc.Order.ListProductsResponse;
import easyfresh.interview.grpc.Order.Empty;
import easyfresh.interview.grpc.Order.Product;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/** Action to perform when ListProducts request comes in. */
public class ListProducts {
  private static final FluentLogger LOG = FluentLogger.forEnclosingClass();
  private final BoneCP dbCxPool;
  private final String dbName;
  /** Acts as a cache store for product list. */
  private static List<Product> products = null;

  public ListProducts(BoneCP dbCxPool, String dbName) {
    this.dbCxPool = dbCxPool;
    this.dbName = dbName;
  }

  public void execute(Empty request, StreamObserver<ListProductsResponse> responseObserver) {
    // Cache the product list...
    if (products == null) {
      try (Connection cx = dbCxPool.getConnection()) {
        PreparedStatement ps = cx.prepareStatement(DatabaseHelper.LIST_PRODUCTS);
        products = DatabaseHelper.selectProducts(dbName, ps, null);
      } catch (SQLException sqlException) {
        LOG.atSevere().withCause(sqlException).log(
            "Exception thrown while trying to get product list from the database");
        responseObserver.onError(
            new StatusException(Status.INTERNAL.withDescription("Failed to list products.")));
        return;
      }
    }

    // Fill and send the response.
    ListProductsResponse.Builder response = ListProductsResponse.newBuilder().clear();
    products.forEach(response::addProducts);
    responseObserver.onNext(response.build());
    responseObserver.onCompleted();
  }
}