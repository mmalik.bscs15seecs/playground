package easyfresh.interview.easyorder.impl;

import com.google.common.collect.Lists;
import com.google.common.primitives.UnsignedInteger;
import easyfresh.interview.grpc.Order;
import easyfresh.interview.grpc.Order.Product;

import java.sql.*;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/** Central class to execute SQL queries relevant to the tables common to order placement action. */
public class DatabaseHelper {
  public static final String LIST_PRODUCTS = "SELECT * FROM `easyfresh-master`.`products`";

  public static final String INSERT_ORDER_DETAIL =
      "INSERT INTO `easyfresh-master`.`orders`"
          + "    (`customer_name`, `delivery_date`, `delivery_address`, `order_taker_name`)\n"
          + "VALUES"
          + "    (?,                ?,               ?,                  ?                )\n";

  public static final String DELETE_ORDER_DETAIL =
      "DELETE FROM `easyfresh-master`.`orders` where `id` = ?\n";

  public static final String INSERT_ORDER_ITEM =
      "INSERT INTO `easyfresh-master`.`order_items`"
          + "    (`order_id`, `product_name`, `quantity`, `total`, `rank`)\n"
          + "VALUES"
          + "    (?,           ?,              ?,          ?,        ?    )\n";

  /** Factory method to get product from the result set. */
  private static Product createProductFromResultSet(ResultSet rs) throws SQLException {
    String name = rs.getString("name");
    String unit = rs.getString("unit");
    float price = rs.getFloat("price");

    Product.Builder productBuilder = Product.newBuilder();
    productBuilder.setName(name);
    productBuilder.setUnit(unit);
    productBuilder.setPrice(price);

    return productBuilder.build();
  }

  // TODO(ammad) Convert this into positional statement.
  /** Maps values into a prepared statement. */
  private static void mapQueryParams(String dbName, PreparedStatement ps, Map<Integer, ?> values)
      throws SQLException {
    if (values != null) {
      for (int key : values.keySet()) {
        Object value = values.get(key);
        // Assuming the key was set correctly. Won't be an issue once positional statements are
        // implemented.
        int pos = key;
        if (value == null) {
          ps.setString(pos, "NULL");
        } else if (value instanceof UnsignedInteger) {
          ps.setLong(pos, ((UnsignedInteger) value).longValue());
        } else {
          ps.setObject(pos, value);
        }
      }
    }
  }

  /**
   * Executes a SELECT statement on Product table.
   *
   * @param dbName Name of the database containing the products table.
   * @param ps Positional Statement with placeholders
   * @param values Values to fill in the SQL query placeholders.
   * @return List of Product objects.
   * @throws SQLException on failure
   */
  public static List<Product> selectProducts(
      String dbName, PreparedStatement ps, Map<Integer, Object> values) throws SQLException {
    List<Product> products = Lists.newArrayList();

    mapQueryParams(dbName, ps, values);

    try (ResultSet rs = ps.executeQuery()) {
      while (rs.next()) {
        Product product = createProductFromResultSet(rs);
        products.add(product);
      }
    }

    return products;
  }

  /**
   * Executes a single INSERT statement and returns the generated key.
   *
   * @param cx Connection to the database.
   * @param dbName Name of the database containing the products table.
   * @param query Statement with placeholders
   * @param values Values to fill in the SQL query placeholders.
   * @return Newly generated key wrapped in an optional.
   * @throws SQLException on failure.
   */
  public static Optional<UnsignedInteger> insert(
      Connection cx, String dbName, String query, Map<Integer, Object> values) throws SQLException {
    PreparedStatement ps = cx.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
    mapQueryParams(dbName, ps, values);

    ps.execute();
    try (ResultSet rs = ps.getGeneratedKeys()) {
      if (rs.next()) {
        UnsignedInteger addedRowId = UnsignedInteger.valueOf(rs.getLong(1));
        return Optional.of(addedRowId);
      }
    }

    return Optional.empty();
  }

  /**
   * Simply executes a query.
   *
   * @param cx Connection to the database.
   * @param dbName Name of the database containing the products table.
   * @param query Statement with placeholders
   * @param values Values to fill in the SQL query placeholders.
   * @throws SQLException on failure.
   */
  public static void execute(
      Connection cx, String dbName, String query, Map<Integer, Object> values) throws SQLException {
    PreparedStatement ps = cx.prepareStatement(query);
    mapQueryParams(dbName, ps, values);

    ps.execute();
  }
}
