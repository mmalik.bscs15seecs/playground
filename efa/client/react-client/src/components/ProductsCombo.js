import React from "react"

const ProductsCombo = ({ products, selection, handleSelectionChanged }) => {
    return (
        <select name="name"  value={selection} onChange={handleSelectionChanged}>
        {products.map((product, index) => (
            <option value={index}>{product.name}</option>
        ))}
        </select>
    )
}

export default ProductsCombo;