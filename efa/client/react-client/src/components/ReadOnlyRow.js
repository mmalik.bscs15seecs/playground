import React from "react"

const ReadOnlyRow = ({ item, handleEditRowBtnClick, handleDeleteRowBtnClick }) => {
    return (
        <tr>
            <td>{item.rank}</td>
            <td>{item.name}</td>
            <td>{item.unit}</td>
            <td>{item.price}</td>
            <td>{item.quantity}</td>
            <td>{item.price * item.quantity}</td>
            <td>
                <button type="button" onClick={(event)=>{ handleEditRowBtnClick(event, item)}}>Edit</button>
                <button type="button" onClick={()=>{ handleDeleteRowBtnClick(item.id)}}>Delete</button>
            </td>
        </tr>
    )
}

export default ReadOnlyRow;