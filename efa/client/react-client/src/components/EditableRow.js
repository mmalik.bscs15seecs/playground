import React from "react"
import "../App.css"
import ProductsCombo from "./ProductsCombo";

const EditableRow = ({ editItem, products, handleEditItemInputChange, handleCancelModifiedRowBtnClick }) => {
    return (
        <tr>
            <td><input className="edit-row-input" type="number" min={1} name="rank" required="required" value={editItem.rank} onChange={handleEditItemInputChange}/></td>
            <td>
                <ProductsCombo products={products} selection={editItem.nameIndex} handleSelectionChanged={handleEditItemInputChange}/>               
            </td>
            <td>{editItem.unit}</td>
            <td>{editItem.price}</td>
            <td><input className="edit-row-input" type="number" min={1} name="quantity" required="required" value={editItem.quantity} onChange={handleEditItemInputChange}/></td>
            <td>{ editItem.price * editItem.quantity }</td>
            <td>
                <button className="asd" type="submit">Save</button>
                <button className="asd" type="button" onClick={handleCancelModifiedRowBtnClick}>Cancel</button>
            </td>
        </tr>
    )
}

export default EditableRow;