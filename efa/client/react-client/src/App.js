import React, { useState, Fragment, useEffect } from "react";
import { nanoid } from 'nanoid'

import "./App.css";

import ReadOnlyRow from "./components/ReadOnlyRow"
import EditableRow from "./components/EditableRow"
import ProductsCombo from "./components/ProductsCombo";

// import mockItems from "./mocks/mock-items.json";
// import mockProducts from "./mocks/mock-products.json";

import OrderServiceProxy from "./OrderServiceProxy";

import Cookies from 'universal-cookie';

const App = () => {   
    const cookies = new Cookies();

    const getOrderConfirmationString = () => {
        return "Total price for the order is Rs." + orderTotalAmount + ". Press 'Ok' to order it or press 'Cancel'to return to the editing screen."
    }

    const onOrderResponse = (msg) => {
        return alert(msg)
    }

    const handleOrderBtnClick = (event) => {
        const orderDetails = { ...details }
        orderDetails.deliverydate = getUnixTimeFromDateString(orderDetails.deliverydate);

        const orderItems = { ...items }

        orderService.placeOrderProxy(orderDetails, orderItems);
    }

    const displayPopUp = (msg) => {
        return window.confirm(msg)
    }
    
    const Comparator = (a, b) => {
        if (a.rank > b.rank) {
            return 1;
        } else if ( a.rank < b.rank ) {
            return -1;
        } else {
            if (a.name > b.name) {
                return 1;
            } else { 
                return -1;
            }
        }
    };

    const orderService = new OrderServiceProxy('http://localhost:8080', onOrderResponse );
    const [orderTotalAmount, setOrderTotalAmount] = useState(0)
    const [products, setProducts] = useState([]);
    const [items, setItems] = useState([])
    const [addItem, setAddItem] = useState({
        id: '',
        rank: '',        
        name: '',
        nameIndex: '',
        unit: '',
        price: '',
        quantity: ''
    });

    const handleAddItemInputChanged = (event) => {
        event.preventDefault();

        const elementName = event.target.getAttribute('name');
        const elementValue = event.target.value;

        const newItem = { ...addItem };
        if (elementName === 'name') {
            newItem.nameIndex = elementValue;    
            newItem.name = products[elementValue].name;
            newItem.unit = products[elementValue].unit;
            newItem.price = products[elementValue].price;
        } else { 
            newItem[elementName] = elementValue;
        }

        setAddItem(newItem)
    }

    const handleAddItemBtnClick = (event) => {
        event.preventDefault();

        
        const newItem = { ...addItem };
        newItem.id = nanoid()

        const newItems = [newItem, ...items]
        newItems.sort(Comparator);

        var newOrderTotalAmount = orderTotalAmount;
        newOrderTotalAmount += (newItem.price * newItem.quantity)

        setItems(newItems);
        setOrderTotalAmount(newOrderTotalAmount)

        
    }

    const [editRowId, setEditRowId] = useState(null);
    const [editItem, setEditItem] = useState({
        rank: '',        
        name: '',
        nameIndex: '',
        unit: '',
        price: '',
        quantity: ''
    });

    const handleEditRowBtnClick = (event, item) => {
        event.preventDefault();
  
        setEditRowId(item.id);
        const currentItem = { 
            rank: item.rank,
            name: item.name,
            nameIndex: item.nameIndex,
            unit: item.unit,
            price: item.price,
            quantity: item.quantity
        };

        setEditItem(currentItem);
    }

    
    const handleDeleteRowBtnClick = (itemId) => {
        const itemIndex = items.findIndex((item) => item.id === itemId);
        const newItems = [...items];

        var newOrderTotalAmount = orderTotalAmount;
        newOrderTotalAmount -= (items[itemIndex].price * items[itemIndex].quantity)

        newItems.splice(itemIndex, 1);        
        setItems(newItems)
        setOrderTotalAmount(newOrderTotalAmount)
    }

    const handleSaveModifiedRowBtnClick = (event) => {
        event.preventDefault();

        const modifiedItem = {
            id: editRowId,
            rank: editItem.rank,
            name: editItem.name,
            nameIndex: editItem.nameIndex,
            unit: editItem.unit,
            price: editItem.price,
            quantity: editItem.quantity
        };

        const itemIndex = items.findIndex((item) => item.id === editRowId);
        const newItems = [...items];
        newItems[itemIndex] = modifiedItem;        
        newItems.sort(Comparator);

        var newOrderTotalAmount = orderTotalAmount;
        newOrderTotalAmount -= (items[itemIndex].price * items[itemIndex].quantity)
        newOrderTotalAmount += (modifiedItem.price * modifiedItem.quantity)

        setItems(newItems);
        setEditRowId(null);
        setOrderTotalAmount(newOrderTotalAmount);
    }

    const handleCancelModifiedRowBtnClick = (event) => {
        setEditRowId(null);
    }

    const handleEditItemInputChange = (event) => {
        event.preventDefault();
        const elementName = event.target.getAttribute('name');
        const elementValue = event.target.value;
        
        const modifiedItem = { ...editItem };

        if (elementName === 'name') {
            modifiedItem.nameIndex = elementValue;
            modifiedItem.name = products[elementValue].name;
            modifiedItem.unit = products[elementValue].unit;
            modifiedItem.price = products[elementValue].price;
        } else { 
            modifiedItem[elementName] = elementValue;
        }
                
        setEditItem(modifiedItem);
    }

    /**
     * Given: new Date() e.g. 2021-10-12T15:21:20.444Z
     * @returns "2021-10-12"
     */
    const getCurrentDateString = () => {  
        var date = new Date()
        var year = date.getFullYear()
        var month = date.getMonth()+1
        var day = date.getDate()
        const dateString = year + "-" + month + "-" + day        
        return dateString;
    }

    const [details, setDetails] = useState({
        customername: 'User1',
        deliverydate: getCurrentDateString(),
        customeraddress: 'Block0, Town 1, City 2',
        ordertaker: 'Worker1'
    })

    /**
     * Given: Date String e.g. 2021-10-12 and xDays e.g. 2 it will
     * @returns a Date after x number of days e.g. 2021-10-12 + 0000-00-02 => 2121-10-12 
     */
    const getNextXDateISOString = (dateStr, xDays) => {
        const date = new Date(Date.parse(dateStr))
        
        const dateAfterTomorrow = new Date(date)
        dateAfterTomorrow.setDate(dateAfterTomorrow.getDate() + xDays)
        
        var year = dateAfterTomorrow.getFullYear()
        var month = dateAfterTomorrow.getMonth()+1
        var day = dateAfterTomorrow.getDate()
        const dateString = year + "-" + month + "-" + day   

        return dateString;  


        //return dateAfterTomorrow.toISOString().split('T')[0];
    }

    const getUnixTimeFromDateString = (dateString) => {
        const date = new Date(Date.parse(dateString))
        return date.getTime() / 1000;
    }

    const handleDetailsChange = (event) => {
        event.preventDefault();

        const elementName = event.target.getAttribute('name');
        const elementValue = event.target.value;

        const newDetails = { ...details };
        newDetails[elementName] = elementValue;

        cookies.set('details', newDetails, [{ path: '/', expires: new Date() }]);

        setDetails(newDetails);
    }

    const productsFillCallback = (arr) => {
        setProducts(arr);
        
        const newAddItem = {
            id: '',
            rank: 1,
            name: arr[0].name,
            nameIndex: 0,
            unit: arr[0].unit,
            price:  arr[0].price,
            quantity: 1            
        };
        
        setAddItem(newAddItem);
    }

    const fillPoductsAndLoadCookie = () => {
        if (products.length === 0) {
            // TODO(ammad): look into passing callback.
            orderService.listProductsProxy(productsFillCallback);
        }
        
        const newDetails = cookies.get('details');

        console.log(newDetails);

        if (newDetails) {
            newDetails.deliverydate = getCurrentDateString();
            setDetails(newDetails); 
        };       
    }

    useEffect(
        fillPoductsAndLoadCookie,
        []
    )


    return (
    <div className="items-container">
        <div className="details">
            <h3>Details:</h3>
            <table>
            <tbody>
                <tr>
                    <td>Customer Name:</td> 
                    <td><input type="text" name="customername"   required="required" onChange={handleDetailsChange} value={details.customername}/></td>
                </tr>
                <tr>
                    <td>Delivery Date:</td> 
                    <td><input type="date" min={getCurrentDateString()} max={getNextXDateISOString(getCurrentDateString(), 2)} value={details.deliverydate} name="deliverydate"   required="required" onChange={handleDetailsChange}/></td>
                </tr>
                <tr>
                    <td>Customer Address:</td> 
                    <td><input type="text" name="customeraddress"    required="required" onChange={handleDetailsChange} value={details.customeraddress}/></td>
                </tr>
                <tr>
                    <td>Order Taker:</td> 
                    <td><input type="text" name="ordertaker" required="required" onChange={handleDetailsChange} value={details.ordertaker}/></td>
                </tr>
            </tbody>
            </table>
        </div>
        
        <div className="add-item">
            <h3>Add an item:</h3>
            <form onSubmit={handleAddItemBtnClick}>
                <input type="number" min={1} name="rank" required="required" value={addItem.rank} onChange={handleAddItemInputChanged}/>
                <ProductsCombo products={products} selection={addItem.nameIndex} handleSelectionChanged={handleAddItemInputChanged}/>
                <input type="text" name="unit" required="required" disabled value={addItem.unit}/>
                <input type="number" name="price" required="required" disabled value={addItem.price}/>
                <input type="number" min={1} name="quantity" required="required" step="0.1" onChange={handleAddItemInputChanged}/>
                <button>Add</button>
            </form>
        </div>
        
        <div className="item-list">
        <form onSubmit={handleSaveModifiedRowBtnClick}>
        <table>
            <thead>
                <tr>
                   <th>Rank</th>
                   <th>Name</th>
                   <th>Unit</th>
                   <th>Unit Price</th>
                   <th>Quantity</th>
                   <th>Amount</th>
                   <th>Action</th>
                </tr>
            </thead>
            <tbody>
            {items.map((item) => (
                <Fragment>                    
                    { (editRowId === item.id) ? (
                    <EditableRow 
                        editItem={editItem}
                        products={products}
                        handleEditItemInputChange={handleEditItemInputChange}
                        handleCancelModifiedRowBtnClick={handleCancelModifiedRowBtnClick}
                    />
                    ) : (
                    <ReadOnlyRow
                        item={item}
                        handleEditRowBtnClick={handleEditRowBtnClick}
                        handleDeleteRowBtnClick={handleDeleteRowBtnClick}
                    />
                    )}
                </Fragment>
            ))}
            </tbody>
        </table>
        </form>
        </div>
        
        <b>Total Amount = {orderTotalAmount}</b>
        <button className="orderBtn" disabled={ (orderTotalAmount > 0 && editRowId === null) ? false : true}
            onClick={e =>
            displayPopUp(getOrderConfirmationString()) && handleOrderBtnClick()
            }
        >
        Order
      </button>
      
    </div>
    
    );
};

export default App;