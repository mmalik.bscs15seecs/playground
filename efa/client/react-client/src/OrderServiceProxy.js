import { OrdersServiceClient } from "./grpc/order_grpc_web_pb"
import { PlaceOrderRequest, Empty, OrderItem } from "./grpc/order_pb"

class OrderServiceProxy {
    constructor(hostUrl, onResult) {
        this.hostUrl = hostUrl;
        this.onResult = onResult;

        this.orderService = new OrdersServiceClient(hostUrl);
    }

    placeOrderProxy(orderDetail, orderItems) {
        const request = new PlaceOrderRequest();

        request.setCustomerName(orderDetail.customername);        
        request.setDeliveryDate(orderDetail.deliverydate);
        request.setCustomerAddress(orderDetail.customeraddress);

        Object.keys(orderItems).forEach((k, i) => {
            var element = orderItems[k];
            const item = new OrderItem();
            item.setProductName(element.name);
            item.setProductQuantity(element.quantity);
            item.setRank(element.rank);
    
            request.addOrderItems(item);    
        });

        this.orderService.placeOrder(request, {}, (err, response) => {
            if (err === null) {
                let respObj = response.toObject();
                if (!respObj) {
                    this.onResult("Failed to place an order.")
                } else {
                    this.onResult("Order created successfully. Order ID is " + respObj.orderId);
                }
            } else {
                this.onResult("An error occurred. Failed to place the order.\n" + err.message)
            }
        });
    }

    listProductsProxy(productsFillCallback) {
        const request = new Empty();
        this.orderService.listProducts(request, {}, (err, response) => {
            if (err === null) {
                let respObj = response.toObject();
                if (respObj) {             
                    productsFillCallback(respObj["productsList"]);
                }
            } else {
                this.onResult("An error occurred. Failed to fetch the products.\n" + err.message)
            }
        });
    }
}

export default OrderServiceProxy;